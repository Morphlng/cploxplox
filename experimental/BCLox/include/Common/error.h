#pragma once
#include <string>
#include <Scanner/token.h>
#include <stdexcept>

class Error : public std::exception
{
public:
    Error(const Token& token, std::string message)
        : line(token.line), column(token.column), message(std::move(message)) {}

    Error(unsigned line, std::string message)
        : line(line), column(0), message(std::move(message)) {}

public:
    const unsigned line;
    const unsigned column;
    const std::string message;
};