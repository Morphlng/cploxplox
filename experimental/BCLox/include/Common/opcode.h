#pragma once
#include <Common/common.h>

enum class OpCode : u8_t
{
    CONSTANT,
    NIL,           // "nil"
    TRUE,          // "true"
    FALSE,         // "false"
    ADD,           // '+'
    SUBTRACT,      // '-'
    MULTIPLY,      // '*'
    DIVIDE,        // '/'
    NEGATE,        // '-'，取否
    NOT,           // '!'，取反
    EQEQ,          // "=="
    NEQ,           // "!="
    GT,            // '>'
    GTE,           // ">="
    LT,            // '<'
    LTE,           // "<="
    POP,           // 立即从栈中推出expression结果
    DEFINE_GLOBAL, // 定义全局变量
    GET_GLOBAL,    // 获取全局变量
    SET_GLOBAL,    // 赋值全局变量
    GET_LOCAL,     // 获取局部变量
    SET_LOCAL,     // 赋值局部变量
    JUMP,          // 无条件跳转
    JUMP_IF_TRUE,  // 成功时跳转
    JUMP_IF_FALSE, // 失败时跳转
    LOOP,          // 循环(往回跳)
    CALL,          // 调用
    RETURN
};