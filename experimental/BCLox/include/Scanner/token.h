#pragma once

#include <string_view>

enum class TokenType
{
    // literals
    PLUS,
    MINUS,
    MUL,
    DIV,
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    COMMA,
    DOT,
    COLON,
    SEMICOLON,
    PLUS_PLUS,
    MINUS_MINUS,
    QUESTION_MARK,

    // bool
    BANG,
    BANGEQ, // ! !=
    EQ,
    EQEQ, // = ==
    GT,
    GTE, // > >=
    LT,
    LTE, // < <=

    // primary
    NUMBER,
    STRING,
    IDENTIFIER,

    // keyword
    // constant
    NIL,
    TRUE,
    FALSE,
    // variable associate
    VAR,
    CLASS,
    THIS,
    SUPER,
    // control statement
    IF,
    ELSE,
    // loop
    FOR,
    WHILE,
    BREAK,
    CONTINUE,
    // function
    FUNC,
    RETURN,
    // logic
    AND,
    OR,

    PRINT,       // 先将print设为语法，实现函数后再修改

    END_OF_FILE, // stop sign
    ERROR
};

struct Token
{
    // 这里简化一点，不记录起始和终止位置了
public:
    static const char* TypeSymbol(TokenType type);
    const char* TypeName();

public:
    TokenType type = TokenType::END_OF_FILE;
    std::string_view lexeme;
    int line = 0;
    int column = 0;
};