#pragma once
#include <string>
#include <string_view>

class VM;

class Runner
{
public:
	static int runScript(const std::string& filename);
	static int runRepl();

public:
	static VM vm;
	static bool DEBUG;

private:
	static int runCode(std::string_view text);
};