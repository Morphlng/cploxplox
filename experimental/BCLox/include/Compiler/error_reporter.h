#pragma once

#include <Common/error.h>
#include <iostream>
#include <string>

class ErrorReporter
{
public:
	void report(const Error& error, bool isRuntime = false);
	void report(int line, int column, std::string message, bool isRuntime = false);
	void reset();
	int count();

private:
	int errorCount = 0;
};