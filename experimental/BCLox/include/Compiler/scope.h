#pragma once

#include <memory>
#include <vector>
#include <Common/opcode.h>
#include <Scanner/token.h>
#include <VM/chunk.h>

enum class FunctionType
{
	None,
	Function,
	Initializer,
	Method
};

class Scope
{
public:
	Scope(FunctionType type, std::unique_ptr<Scope> enclosing = nullptr);
	void beginScope();
	void endScope();

	void declareLocal(const Token &name);
	void defineLocal();
	void addLocal(const Token &name);
	int resolveLocal(const Token &name);

	// 局部变量
	struct Local
	{
		Token name;
		size_t depth;
		bool defined;

		Local(const Token &name, size_t depth, bool defined = false) : name(name), depth(depth), defined(defined) {}
	};

public:
	FunctionType type;
	std::unique_ptr<Scope> enclosing;
	std::unique_ptr<Chunk> chunk;
	size_t scope_depth;
	std::vector<Local> locals;
};