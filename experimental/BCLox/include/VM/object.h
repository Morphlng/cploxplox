#pragma once

#include <memory>
#include <functional>
#include <VM/chunk.h>

class FunctionObject
{
public:
	FunctionObject(std::string name, int arity, std::unique_ptr<Chunk> chunk = nullptr);

public:
	std::string name;
	int arity;
	std::unique_ptr<Chunk> chunk;
};

class NativeFunction
{
	using Func = std::function<Value(std::vector<Value>)>;

public:
	NativeFunction(std::string name, int arity, Func callable);

public:
	std::string name;
	int arity;
	Func function;
};