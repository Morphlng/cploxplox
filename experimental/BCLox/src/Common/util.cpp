#include <Common/util.h>
#include <fstream>

void print(const char* s)
{
	while (*s)
	{
		if (*s == '%' && *++s != '%')
			throw std::runtime_error("invalid format string: missing arguments");
		std::cout << *s++;
	}
}

std::optional<std::string> readfile(const std::string& file_path)
{
    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit);
    try
    {
        ifs.open(file_path);
        std::string text((std::istreambuf_iterator<char>(ifs)),
            (std::istreambuf_iterator<char>()));
        ifs.close();
        return text;
    }
    catch (const std::exception& e)
    {
        return std::nullopt;
    }
}
