#include <Compiler/error_reporter.h>
#include <iomanip>
#include <iostream>

constexpr auto resetText = "\033[0m";
constexpr auto redText = "\033[31m";
constexpr auto greyText = "\033[90m";

void ErrorReporter::report(const Error& error, bool isRuntime)
{
	report(error.line, error.column, error.message, isRuntime);
}

void ErrorReporter::report(int line, int column, std::string message, bool isRuntime)
{
    const auto stage = isRuntime ? "Runtime" : "Syntax";
    std::cerr
        << redText << std::setw(8) << stage << " Error  " << resetText
        << message
        << greyText << " (" << line << ':' << column << ")\n" << resetText;

    errorCount++;
}

void ErrorReporter::reset()
{
    errorCount = 0;
}

int ErrorReporter::count()
{
    return errorCount;
}
