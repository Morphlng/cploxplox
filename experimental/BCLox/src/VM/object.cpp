#include <VM/object.h>

FunctionObject::FunctionObject(std::string name, int arity, std::unique_ptr<Chunk> chunk) :name(std::move(name)), arity(arity), chunk(std::move(chunk))
{}

NativeFunction::NativeFunction(std::string name, int arity, Func callable) : name(std::move(name)), arity(arity), function(callable)
{}
