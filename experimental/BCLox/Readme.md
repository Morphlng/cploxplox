# BCLox

## 一、介绍

Bytecode version of [Lox](https://github.com/munificent/craftinginterpreters) written in C++17

Still **Work in Progress**

## 二、 软件架构

1. Scanner，承担词法分析的功能，对输入的源代码进行分词，产生对应的Token
2. Compiler，承担语法分析、中间代码生成的功能，从Scanner处获取Token，按照语法进行分析，并发射(emit)对应的字节码
3. VM，Virtual Machine，执行编译出的字节码。

## 三、依赖

`Chunk`中使用了[yas](https://github.com/niXman/yas)，来实现将字节码序列化到文件的功能，你需要下载并配置此submodule，更多关于yas的使用教程，可以看我给出的[示例](https://gitee.com/Morphlng/utility/blob/master/C++/yas-7.1.0/example.cpp)。

> 该功能暂时不可用

## 四、进度

>  由于[cploxplox](https://gitee.com/Morphlng/cploxplox)已经相当完善，因此接下来一段时间我会着手其他事物。该项目被标为Experimental，寓意着不会投入太多精力，可能不会继续开发。

- [x] 基本类型（Number、String、nil）
- [x] 算术运算（+、-、*、/）
- [x] 全局变量
- [x] 局部变量
- [x] 控制流（判断语句，循环语句）
- [x] 函数支持（自定义函数，内置函数）
- [x] 三元表达式
- [x] 自增、自减
- [ ] OOP
