# Lox Grammar

文法将被具体地划分为语句(Statement)和表达式(Expression)。其中，表达式应当生成值，而语句用来包含一系列表达式。

## Program

```javascript
program         => declaration * EOF;
```

## Declarations

```javascript
declaration     => varDecl
                |  funcDecl
                |  classDecl
                |  statement ;
// allow: var a,b=1,c="hello",...;
varDecl         => "var" IDENTIFIER ("=" expression )?
                   (, IDENTIFIER ("=" expression )?)* ";" ;
funcDecl        => "func" function ;
classDecl       => "class" IDENTIFIER ( ">" IDENTIFIER )? "{" function* "}" ;
```

## Statements

```javascript
statement       => exprStmt
                |  ifStmt
                |  whileStmt
                |  forStmt
                |  breakStmt
                |  continueStmt
                |  returnStmt
                |  importStmt
                |  block
exprStmt        => expression ";" ;
ifStmt          => "if" "(" expression ")" statement
                ( "else" statement )? ;
whileStmt       => "while" "(" expression ")" statement ;
forStmt         => "for" "(" (varDecl | exprStmt | ";")
                expression? ";"
                expression? ")" statement ;
breakStmt       => "break" ";" ;
continueStmt    => "continue" ";" ;
returnStmt      => "return" expression? ";" ;
importStmt      => "import" "{" ("*" | (IDENTIFIER "as" IDENTIFIER)+) "}" "from" STRING ";" ;
block           => "{" declaration* "}" ;
```

## Expressions

```javascript
expression      => comma ;
comma           => assignment (, assignment)* ;
assignment      => (call ".") IDENTIFIER ("=" | "+=" | "-=" | "*=" | "/=") assignment  
                |  ternary ;
ternary         => logic_or ( "?" expression ":" ternary)? ;
logic_or        => logic_and ("or" logic_and)* ;
logic_and       => equality ("and" equality)* ;
equality        => comparison ( ( "!=" | "==" ) comparison )* ;
comparison      => term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
term            => factor ( ( "-" | "+" ) factor )* ;
factor          => unary ( ( "/" | "*" ) unary )* ;
unary           => ( "!" | "-" ) unary | prefix ;
prefix          => ("++" | "--") call | postfix ;
postfix         => call ("++" | "--")? ;
call            => primary ( "(" arguments? ")" | "." IDENTIFIER | "[" logic_or "]")* ;
primary         => "true" | "false" | "nil" | "this" | "super" "." IDENTIFIER
                | NUMBER | STRING | IDENTIFIER | "(" expression ")"
                | lambda | list
lambda          => "func" "(" parameters? ")" block;
list            => "[" arguments "]" ;
```

## Utility

这一部分提取出了一些本身不是语法树上的节点，但常常被使用的语法

```javascript
arguments       => ternary ( "," ternary )* ;
function        => IDENTIFIER "(" parameters? ")" block ;
// parameters中可以包含默认值，但默认值必须出现在后置位
parameters      => IDENTIFIER (= ternary)? ( "," IDENTIFIER (= ternary)?)* ;
```

## Last Update

2021-12-25 15:50:11