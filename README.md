# cploxplox

## 一、介绍

A C++ implementation of a self-designed language Cploxplox (Cxx).

## 二、软件架构

1. Lexer，词法分析器。用于将输入语句划分为Token
2. Parser，语法分析器。接受Lexer生成的Token_list，生成AST树。
2. Resolver，语义分析器。对AST树进行静态分析，完成变量绑定等工作
3. Interpreter，解释器。解释AST树定义的行为

## 三、安装教程

1. 在根目录执行`make`
2. output文件夹中将生成`cploxplox.exe`
3. `make clean`可用于清除`.o`文件
4. `make clean_all`可用于清除包括可执行文件在内的所有编译生成文件

## 四、语法说明

> 完整的语法定义见: grammar.md

不同于[basic](https://gitee.com/Morphlng/basic)时，语法杂糅在一起，在编写[cploxplox](https://gitee.com/Morphlng/cploxplox)时，我将整个语法细分为三大部分：`声明(Declaration)`、`语句(Statement)`、`表达式(Expression)`。

1. 声明。声明主要用于`定义数据对象`，`变量声明`、`类定义`、函数定义都属于声明。若无法匹配上述声明，则此时退为声明`语句`。

2. 语句。语句主要用于各种`控制流`，包括判断语句、循环语句等，语句是一系列表达式的合集。

3. 表达式。每个表达式都将返回一个对象，表达式包括`赋值语句`、`数学运算`、`调用语句`、`索取语句`、`变量`。

### 4.1 Declarations

#### 4.1.1 变量声明

```javascript
lox > var a = 1;
lox > var b;
```

以`var`关键字打头的语句为`变量声明语句`，你可以在声明的同时赋予初值，也可以只进行声明。变量**必须**声明才可以使用，这点不同于JavaScript/Python。

#### 4.1.2 函数声明

```javascript
lox > func add(a,b) {
...     return a+b;        
...   }
```

以`func`关键字打头的语句*可以*为`函数声明语句`，若func后没有紧跟`函数名`，则为定义`匿名函数`。

#### 4.1.3 类声明

```javascript
lox > class A {
...     init() {
...       this.name = "A";
...     }
...     
...     hi() {
...       print(this.name); # 数据成员需显示调用
...     }
...   }
...
lox > class B > A {
...     hi() {
...       super.hi();
...       print("B");
...     }
...   }
...
lox > var b = B();
lox > b.hi();
"A"
"B"
```

### 4.2 Statements

#### 4.2.1 block

block是一种作用域的概念，由"{}"包括起来的位置将产生一个全新的作用域，在该处可以定义外部已经声明过的变量，而不会覆盖。一般情况下block是作为其他Statement的附带品，但也可以单独使用。

```javascript
lox > var a = "global";
...   {
...     func showA() {
...       print(a);
...     }
...     
...     showA();
...     var a = "block";
...     showA();
...     print(a);
...   }
"global"
"global"
"block"
```

> 上述例子同时展示了block的作用，以及**词法作用域**（详情请查询， Lexical Scoping）

#### 4.2.2 ifStmt

Lox在设计时遵循一切从简，因此没有将控制语句设计的复杂，if和else已经足以进行嵌套了（符合C++的语法）

```javascript
lox > func Range(age) {
...     if (age < 18) {
...       return "Teen";
...     }
...     else if (age < 35) {
...       return "Programmar";
...     }
...     else {
...       return "Retired";
...     }
...   }
...
lox > Range(16);
...
Kid
lox > Range(19);
...
Programmar
lox > Range(87);
...
Retired
```

> Lox不像Python，你不能省略()

#### 4.2.3 forStmt

目前提供了两种基本的遍历方式，for循环与while循环，语法遵循C++

```javascript
lox > for(var i = 0; i < 3; i+=1){
...   	print(i);
...   }
...
0
1
2
```

> 注意，for循环体的第一部分的赋值，将被定义在新的{}中。

#### 4.2.4 WhileStmt

```javascript
lox > var i = 0;
lox > while(i < 3){
...     print(i++);	
...   }
...
0
1
2
```

> 正如你所见，cploxplox支持++、--、+=、-=、*=、/=

#### 4.2.5 break/continue Stmt

cploxplox支持从循环中跳出或跳过当前一轮循环，使用方法与其他语言一致。

```javascript
lox > for(var i = 0; i < 10; i++){
...		if(i > 5) break;
...		if(i == 3) continue;
...   	print(i);
...   }
...
0
1
2
4
5
```

#### 4.2.6 returnStmt

在演示函数定义时我们已经用到了return语句。你可以return一个值，或者nothing。

> 注意，在构造函数中不可以return任何值。（换言之构造函数永远return nil）

#### 4.2.7 importStmt

最新版本的cploxplox加入了模块概念，就像python的import、c++的include，你可以引用另一个lox文件中定义的函数、类、与全局变量：

```typescript
lox > import { add as a, sub as s} from "math.lox"
lox > a(1,2);
3
// or
lox > import { * } from "math.lox"
lox > add(1,2);
3 
```

### 4.3 Expression

Expression 主要是进行各种运算，但也包含赋值、比较、以及调用函数等操作。这里挑一部分演示

#### 4.3.1 Assignment

可以为一个**已经声明过**的变量赋值，也可以为一个类实例添加字段。计划加入列表后，给列表元素重新赋值的语法：

```javascript
lox > var a = 1;
lox > a += 10;
11
lox > class Person{}
lox > var me = Person();
lox > me.name = "Dave"
```

#### 4.3.2 Lambda

如果func后没有名字，直接是(参数)，则为定义lambda函数

```javascript
lox > func (a,b){return a+b;}(1,2);
3
```

> Lambda是表达式，因此他会返回一个对象，你可以将其赋值给一个变量。根据我在Grammar中的定义，Lambda在定义时就可以被调用，也就是上面例子中这样。

## 五、内置函数与内置类

由于篇幅限制，此部分请见[scripts/Built_in_Expansion_Guide.md](./scripts/Built_in_Expansion_Guide.md)

## 六、命令行参数

为了方便外部调用，新添加了命令行参数，具体如下：

```bash
$ ./cploxplox -h
Welcome to cploxplox!
Usage: ./cploxplox [options...]

Options:
        -f,--file : Execute Lox script from given file_path [default: none]
               -i : A flag to toggle interactive mode [implicit: "true", default: false]
     -v,--verbose : A flag to toggle verbose [implicit: "true", default: false]
       -D,--Debug : A flag to toggle debug mode [implicit: "true", default: false]
        -h,--help : print help [implicit: "true", default: false]
```

当不使用命令行参数，或显示调用`-i`时，将进入交互模式。

> 该实现依赖于[Argparse](https://github.com/morrisfranken/argparse)，这是一个Header-only的命令解析库，具体使用方法见其仓库

## 七、参与贡献

1.  Fork 本仓库
2.  新建分支
3.  为该语言添加新的feature
4.  提交代码
5.  新建 Pull Request
